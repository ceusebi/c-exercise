#include "timestamp.h"

namespace cexercise {

  Timestamp::Timestamp() {
    this->pollMachineId();
  }

  Timestamp::~Timestamp() {

  }

  const std::string Timestamp::key() const {
    return Timestamp::encode(this->pollProcessor());
  }

  void Timestamp::pollMachineId() {
    // Sinchronization goes here.
    m_machineId = Timestamp::machineId();
  }

  const Timestamp::strTimestamp Timestamp::pollProcessor() const {
    Timestamp::strTimestamp key;
    key.machineId = m_machineId;
    // Processor function goes here.
    return key;
  }

  const std::string Timestamp::encode(const Timestamp::strTimestamp key) {
    std::string encoded = "Hello!";
    // Encoding goes here.
    return encoded;
  }

  // Dummy function.
  const uint8_t Timestamp::machineId() {
    return 3;
  }

}

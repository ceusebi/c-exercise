#pragma once
#include <stdint.h>
#include <string>

namespace cexercise {

  class Timestamp {

  private:
    typedef struct {
      uint64_t time;
      uint32_t cycles;
      uint8_t processorId;
      uint8_t machineId;
    } strTimestamp;


  private:
    uint8_t m_machineId;

  public:
    Timestamp ();
    virtual ~Timestamp ();

  #pragma region Public interface

  public:
    const std::string key() const;

  #pragma endregion Public interface

  #pragma region Private interface

  private:
    void pollMachineId();
    const strTimestamp pollProcessor() const;
  #pragma endregion Private interface

    static const std::string encode(const strTimestamp key);
    // Dummy function.
    static const uint8_t machineId();

  };

}
